
# Zahnartzpraxis

## Frontend

- Html
- CSS
- JavaScript

## framework

- Angular






## APIs

[Google Embed API]("https://developers.google.com/maps/documentation/embed/get-started")

[Google Sign-In]("https://www.google.com/search?channel=fs&client=ubuntu&q=google+login+api")
## Home

![Home](https://git.thm.de/zyom52/hausuebung-2/-/raw/master/screenschots/home.png)

## Über uns

![Über uns](https://git.thm.de/zyom52/hausuebung-2/-/raw/master/screenschots/Ueberuns.png)

## Service

![Service](https://git.thm.de/zyom52/hausuebung-2/-/raw/master/screenschots/Service.png)

## Weg

![Weg](https://git.thm.de/zyom52/hausuebung-2/-/raw/master/screenschots/localhost_4200_weg(iPhone%206_7_8).png)

## Sign In

![Sign in](https://git.thm.de/zyom52/hausuebung-2/-/raw/master/screenschots/Signin.png)

## Authors

- [Zakaria Y.O.Mohammedahmed](https://git.thm.de/)

