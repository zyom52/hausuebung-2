import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatenschuetzComponent } from './datenschuetz.component';

describe('DatenschuetzComponent', () => {
  let component: DatenschuetzComponent;
  let fixture: ComponentFixture<DatenschuetzComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatenschuetzComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DatenschuetzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
