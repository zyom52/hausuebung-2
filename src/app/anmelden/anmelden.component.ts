// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-anmelden',
//   templateUrl: './anmelden.component.html',
//   styleUrls: ['./anmelden.component.css']
// })
// export class AnmeldenComponent implements OnInit {

//   constructor() { }

//   ngOnInit(): void {
//   }

// }

import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { GoogleServiceService } from '../google-service.service';


@Component({
  selector: 'app-anmelden',
 templateUrl: './anmelden.component.html',
 styleUrls: ['./anmelden.component.css']
})
export class AnmeldenComponent implements OnInit {

  user!: gapi.auth2.GoogleUser; 
  constructor(private anmelden: GoogleServiceService, private ref: ChangeDetectorRef) {
    

  }

  ngOnInit(): void {
    this.anmelden.observable().subscribe((user: any) => {
      this.user = user
      this.ref.detectChanges()
    })


  }

  signIn() {
    this.anmelden.signin()
  }

  signOut() {
    this.anmelden.signOut()
  }
}




