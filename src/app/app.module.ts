import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { AppointmentComponent } from './appointment/appointment.component';
import { BannerComponent } from './banner/banner.component';
import { CarouselComponent } from './carousel/carousel.component';
import { ContactComponent } from './contact/contact.component';
import { DatenschuetzComponent } from './datenschuetz/datenschuetz.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ServiceComponent } from './service/service.component';
import { AnmeldenComponent } from './anmelden/anmelden.component';
import { OAuthModule } from 'angular-oauth2-oidc';
import { HttpClientModule } from '@angular/common/http';
import { WegComponent } from './weg/weg.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    AppointmentComponent,
    BannerComponent,
    CarouselComponent,
    ContactComponent,
    DatenschuetzComponent,
    FooterComponent,
    HomeComponent,
    NavbarComponent,
    ServiceComponent,
    AnmeldenComponent,
    WegComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    // OAuthModule.forRoot
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
