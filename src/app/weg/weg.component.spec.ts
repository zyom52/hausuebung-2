import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WegComponent } from './weg.component';

describe('WegComponent', () => {
  let component: WegComponent;
  let fixture: ComponentFixture<WegComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WegComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
