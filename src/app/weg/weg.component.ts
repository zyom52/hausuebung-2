import { Component, OnInit } from '@angular/core';
import { Loader } from '@googlemaps/js-api-loader';
import { map } from 'rxjs';

@Component({
  selector: 'app-weg',
  templateUrl: './weg.component.html',
  styleUrls: ['./weg.component.css']
})
export class WegComponent  {

  constructor() { }
 

   ngOnInit(): void {
    let loader = new Loader({
      apiKey: 'AIzaSyBLdoZBKg3WW2G48z-mmbSUTqfyTsCLBAI'
    })

    loader.load().then(() => {
    const map =  new google.maps.Map(document.getElementById("map") as HTMLElement, {
        center: { lat: -34.397, lng: 150.644 },
        zoom: 6,
      });
    });

    const marker = new google.maps.Marker({
      position: { lat: -34.397, lng: 150.644 } ,
     
    });

  } 

}


