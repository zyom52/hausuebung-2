import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { AnmeldenComponent } from './anmelden/anmelden.component';
import { AppointmentComponent } from './appointment/appointment.component';
import { ContactComponent } from './contact/contact.component';
import { DatenschuetzComponent } from './datenschuetz/datenschuetz.component';
import { HomeComponent } from './home/home.component';
import { ServiceComponent } from './service/service.component';
import { WegComponent } from './weg/weg.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'kontakt', component: ContactComponent },
  { path: 'about', component: AboutComponent },
  { path: 'service', component: ServiceComponent },
  { path: 'daten', component: DatenschuetzComponent },
  { path: 'appointment', component: AppointmentComponent },
  { path: 'anmelden', component: AnmeldenComponent },
  { path: 'weg', component: WegComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }

export const routingComponents = [HomeComponent, ServiceComponent, AboutComponent,
  AppointmentComponent,
  ContactComponent, DatenschuetzComponent, AnmeldenComponent,WegComponent 
  
];
